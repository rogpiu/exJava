package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe uma palavra do terminal e determina
 * se ela é um palíndromo.
 * 
 * Exs: 
 * 
 * input: ovo
 * output: A palavra ovo é um palíndromo
 * 
 * input: jose
 * output: A palavra jose não é um palíndromo 
 */
public class Palindromo {
	public static void main(String args[]) {
		
		Scanner s = new Scanner(System.in);
		System.out.println("digite um nome: ");
		String palavra = s.nextLine();
		String palindromo = "";
		for (int i=palavra.length(); i>0; i--) {
			palindromo += palavra.charAt(i-1);
		}
		
		if (palindromo.equals(palavra)) {
			System.out.println("E palindromo");
		}else {
			System.out.println("Nao e palindromo");			
		}
	}
}

