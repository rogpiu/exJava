package a_basicos;

public class Primo {
	/**
	 * Crie um programa que imprima todos os números
	 * primos de 1 à 100
	 */
	public static void main(String[] args) {
		
		
		boolean primo = true;
		int divisor;
		
		for (int i = 100; i > 0; i--) {
			
			int num = i;
			divisor = --num;
			
			while(divisor > 1) {
			    int resto = (i % divisor);
			    if (resto == 0) {
			    	primo = false;
			    	divisor = 0;
			    } else {
			    	divisor--;
			    }
			}
			
			if (primo) {
				System.out.println("Numero e primo " + i);
			}
			primo = true;
		}
		
		
		
	}
}
