package a_basicos;

/**
 * Crie um programa que imprima todas as cartas do baralho.
 * 
 * Exemplo: 
 * 
 * Ás de Ouros
 * Ás de Espadas
 * Ás de Copas
 * Ás de Paus
 * Dois de Ouros
 * ...
 * 
 */
public class Baralho {
public static void main(String[] args) {
	

String[] cartas = {"As", "Dois", "Tres", "Quatro", "Cinco", "Seis", "Sete","Oito", "Nove", "Dez", "Dama","Valete", "Reis"};
String[] nipes = {"Ouro", "Espada", "Copas","Paus"};

for (String carta: cartas) {
	for(String nipe: nipes) {
		System.out.println(carta + " de " + nipe);
	}
}
}
}