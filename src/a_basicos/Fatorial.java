package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe um número do usuário
 * e imprime a fatorial desse mesmo número.
 * 
 */
public class Fatorial {
	public static void main(String args[]) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Digite um numero para calculo fatorial ");
		
		int resultado = 1; 
		int fatorial = scanner.nextInt();
		
		  for (int i=fatorial; i>0; i--) {
 			  resultado = resultado * i;
	      }
			  System.out.println(resultado);
	}
}
