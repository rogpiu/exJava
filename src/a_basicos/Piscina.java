package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe as três dimensões de uma piscina
 * (largura, comprimento, profundidade) e retorne a sua capacidade
 * em litros.
 */
public class Piscina {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Digite a largura: ");
		int largura = scanner.nextInt();
		
		System.out.println("Digite a comprimento: ");
		int comprimento = scanner.nextInt();
		
		System.out.println("Digite a profundidade: ");
		int profundidade = scanner.nextInt();
		
		int volume = largura * comprimento * profundidade;
		int litros = volume * 1000;
		
		System.out.println("O volume da piscina e " + litros);
		
		scanner.close();
		
	}
}

