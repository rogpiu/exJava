package b_intermediarios;

import java.util.Random;

/**
 * Crie um programa que simula o funcionamento de um caça níquel, da seguinte forma:
 * 
 * 1- Sorteie 3x um valor dentre as strings do vetor de valores
 * 2- Compare os valores sorteados e determine os pontos obtidos usando o
 * seguinte critério:
 * 
 * Caso hajam três valores diferentes: 0 pontos
 * Caso hajam dois valores iguais: 100 pontos
 * Caso hajam três valores iguais: 1000 pontos
 * Caso hajam três valores "7": 5000 pontos
 * 
 */
public class CacaNiquel {
	public static void main(String args[]) {
		String[] valores = {"abacaxi", "framboesa", "banana", "pera", "7"};
		int sorteio;
		
		Random random = new Random();
		int sorteio1 = random.nextInt(5);
		System.out.println(valores[sorteio1]);
		
		int sorteio2 = random.nextInt(5);
		System.out.println(valores[sorteio2]);
		
		int sorteio3 = random.nextInt(5);
		System.out.println(valores[sorteio3]);
		
		if((sorteio1 == sorteio2) && (sorteio2 == sorteio3)) {
			if (sorteio1==4) {
				System.out.println("Voce ganhou 5000 pontos");
			}else {
				System.out.println("Voce ganhou 1000 pontos");
			}

		}else {
			if(((sorteio1 == sorteio2) && (sorteio2 != sorteio3)) || 
					((sorteio1 == sorteio3) && (sorteio3 != sorteio2)) ||
					((sorteio2 == sorteio3) && (sorteio2 != sorteio1))) {
				System.out.println("Voce ganhou 100 pontos");
			}else {
				System.out.println("Voce ganhou 0 pontos");
			}

		}
	}
}
