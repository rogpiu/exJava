package b_intermediarios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Crie um programa que recebe um todo list e a escreve em um arquivo txt.
 * 
 * A cada iteração, o sistema deve pedir uma  única tarefa e exibir a possibilidade
 * de encerrar o programa.
 * 
 * Quando o usuário encerrar o programa, deve-se gerar o arquivo txt com as tarefas
 * que foram inseridas.
 *
 */
public class ListaTarefas {
	public static void main(String[] args) {
		Path path = Paths.get("teste.txt");
		String umString = "Esse e meu arquivo de teste";
		
		boolean listar = true;
		String listaTarefa = "";
		int qtdTarefa = 0;
		
		while(listar) {
			Scanner scanner = new Scanner(System.in);
			System.out.print("Digite uma tarefa ou 'S' para sair");
			String tarefa = scanner.nextLine();
			if (tarefa.equalsIgnoreCase("S")) {
				listar = false;
			}else {
				qtdTarefa++;
				listaTarefa += "Tarefa " + qtdTarefa + " : "+ tarefa + "\n";
			}
		}
		
		try {
			Files.write(path, listaTarefa.getBytes());
		}catch (IOException e) {
			e.printStackTrace();
		}
				
	}
}

