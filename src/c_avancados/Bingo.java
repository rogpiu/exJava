package c_avancados;

import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

/**
 * Crie um programa que realiza o sorteio de 20 números
 * de 1 à 50, de forma que um número sorteado não pode ser repetido.
 * 
 * Após o sorteio, o progama deve conferir uma cartela pré-determinada
 * e determinar o resultado dentre as possíveis opções:
 * 
 * - Bingo: cartela cheia
 * - Linha: linha cheia (vertical ou horizontal)
 * - Erro: nenhuma das anteriores
 *
 * O programa deve imprimir os números sorteados e o resultado.	
 */
public class Bingo {

	public static void main(String args[]) {
		int[] sorteio = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,29};
		
		
		Set<Integer> numeros = new TreeSet<Integer>();
		Random rand = new Random();
		
		//Vamos sortear 20 números de 0 até 100 sem repetição
		while (numeros.size()< 20) {
		   numeros.add(rand.nextInt(51));
		}
		
		

		int[][] cartela = {
				{10, 21, 34, 43},
				{7, 15, 11, 50},
				{41, 9, 33, 2},
				{1, 2, 34, 49}
		};
		
		Iterator<Integer> numSortIter = numeros.iterator();

		System.out.print("Numero sorteado :" );
		while(numSortIter.hasNext()) {
			System.out.print(numSortIter.next() + ", ");
		}
		System.out.println("\n");
		System.out.print("Numero encontrato na cartela :");
		for (int i = 0; i < 4; i++) {
			for (int k = 0; k < 4; k++) {
				Iterator<Integer> sorteioIter = numeros.iterator();
				while(sorteioIter.hasNext()) {
					if (cartela[i][k] == sorteioIter.next() ) {
						System.out.print(cartela[i][k]  + ", ");
					}
				}
			}
		}
	}
}
