package c_avancados;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Saque {

	public static void main(String[] args) {

		//		ArrayList<String> lista = new ArrayList<>();

		//		lista.add("Uma Palavra");
		//		lista.get(0);
		//		lista.size();
		//		lista.remove(0);

		Path path = Paths.get("contas.csv");

		String regravacao = "";

		Scanner scanner = new Scanner(System.in);
		System.out.println("Entre com o numero da conta: ");
		int conta = scanner.nextInt();


		Scanner scanner2 = new Scanner(System.in);
		System.out.println("Entre com um valor para saque:  ");
		int valor = scanner.nextInt();


		try {
			List<String> lista = Files.readAllLines(path);
			regravacao = lista.get(0) + "\n";
			lista.remove(0);
			boolean encontrado = false;

			for (String linha: lista) {

				String[] partes = linha.split(",");

				int contaBanco = Integer.parseInt(partes[0]);
				int valorBanco = Integer.parseInt(partes[4]);

				if(contaBanco == conta) {
					System.out.println("Conta digitada: " + conta);
					System.out.println("valor digitada: " + valor);
					System.out.println("valor banco: " + valorBanco);
					encontrado = true;
					if (valorBanco == 0) {
						System.out.println("Conta sem saldo");
						regravacao += partes[0]+ "," + partes[1]+ "," + partes[2]+ "," +partes[3]+ "," + partes[4]+ "\n";
					}else {
						if (valor <= valorBanco) {
							valorBanco -= valor;
							regravacao += partes[0]+ "," + partes[1]+ "," + partes[2]+ "," +partes[3]+ "," + valorBanco+ "\n";							
						}else {
							System.out.println("Saldo insuficiente, valor em conta: " + valorBanco);
							regravacao += partes[0]+ "," + partes[1]+ "," + partes[2]+ "," +partes[3]+ "," + partes[4]+ "\n";
						}
					}

				}else {
					if (contaBanco != conta) {

						regravacao += partes[0]+ "," + partes[1]+ "," + partes[2]+ "," +partes[3]+ "," + partes[4]+ "\n";
					}
				}

			}

			if (!encontrado) {
				System.out.println("Conta nao existente = " + conta);
			}

			Files.write(path, regravacao.getBytes());
			//			System.out.println(regravacao);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
